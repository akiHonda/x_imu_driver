#!/bin/bash
RELPATH=/tmp/libximu_release
rm -rf $RELPATH
mkdir  $RELPATH
cd     $RELPATH
svn co https://opensource.cit-ec.de/svn/libximu/trunk/ libximu
tar czf libximu_rel.tar.gz --exclude='libximu/make_release.sh' --exclude-vcs libximu 
