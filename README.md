x_imu_driver
======================
This package provides an interface between ROS and CITEC's libximu (https://opensource.cit-ec.de/projects/libximu/wiki).
The libximu is an open source c++ re-implementation of the x-IMU API. This package aims to use X-IO Technologies's x-IMU within ROS, and send sensor data to topics and sned some commands to sensor.

Usage
------  
* Build  
`catkin_make`  
* (option: If you'd like to use rviz.rviz) Install below package  
`sudo apt-get install ros-indigo-imu-tools`  
* Become root to use serial port.  
`sudo bash`  
* Launch sensor node.  
`roslaunch x_imu_driver imu_bringup.launch`


Parameters
----------------
<param name="freqency" value="100" />  
<param name="frame" value="inu" />  
<param name="topic_name" value="inu" />  

Published Topics
----------------
imu(sensor_msgs/Imu)  

Information
--------
N/A
 
Licences ライセンス
----------
TODO

