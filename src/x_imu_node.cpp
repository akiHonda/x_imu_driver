#include "ros/ros.h"
#include <sstream>
#include "libximu/XIMU.h"
#include "sensor_msgs/Imu.h"
#include "tf/transform_datatypes.h"
#include <iostream>
#include <string>
#include <Eigen/Geometry>

float FixedToFloat(unsigned int fixedValue, unsigned int q)
{
  return ((float)(fixedValue) / (float)(1 << ((int)q)));
}

//TODO
// https://opensource.cit-ec.de/projects/libximu/wiki
//inline int toInt(std::string s) {int v; std::istringstream sin(s);sin>>v;return v;}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "x_imu_node");
  ros::NodeHandle n;
  ros::NodeHandle private_nh("~");

  //double yaw_offset = -90.0;
  double yaw_offset = 0.0;

  // root check
  if(geteuid() != 0)
    {
      ROS_FATAL("[x_imu_node] ERROR: needed to be root");
      exit(-1);
    }

  std::string AlgorithmModes_str[] = {"Disabled", "IMU", "AHRS"};
  int DataOutputRates_int[] = {0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512};
  
  /// ROS Param
  // Port name
  std::string port;
  private_nh.param("port", port , std::string("/dev/ttyUSB0"));

  /// ROS Params
  // Loop rate
  std::string frame;
  private_nh.param("frame", frame , std::string("imu_link"));

  // Topic name
  std::string topic_name;
  private_nh.param("topic_name", topic_name , std::string("/imu"));

  // Prepare publisher
  ros::Publisher imu_pub = n.advertise<sensor_msgs::Imu>(topic_name, 1000);

  private_nh.param("yaw_offset", yaw_offset , 0.0);

  bool debug_msg = false;
  private_nh.param("debug_msg", debug_msg , false);

  bool init_ahrs = false;
  private_nh.param("init_ahrs", init_ahrs , false);

  bool clear_tare = false;
  private_nh.param("clear_tare", clear_tare , false);


  //intantiate XIMU object                                                                                                                                                                             
  //XIMU ximu(argv[1], XIMU::XIMU_LOGLEVEL_LOG);
  XIMU ximu( port.c_str() , XIMU::XIMU_LOGLEVEL_LOG);


  /// Read data from xIMU
  //set loglevel                                                                                                                                                                                       
  if(argc == 3){
    ROS_INFO("[x_imu_node] requested debug loglevel");
    ximu.set_loglevel(XIMU::XIMU_LOGLEVEL_DEBUG);
  }

  //scan for device & exit on failure                                                                                                                                                                  
  if (!ximu.get_device_detected()){
    ROS_FATAL("[x_imu_node] ERROR: no device found?!");
    exit(-1);
  }

  unsigned int deviceid;
  // get decviceid:                                                                                                                                                                              
  if (!ximu.get_register(XIMU::REGISTER_ADDRESS_DeviceID, &deviceid)){
    ROS_FATAL("[x_imu_node] ERROR: cant fetch device ID");
    exit(-1);
  }


  // Set rate
  int data_rate;
  private_nh.param("data_rate", data_rate , 128);
  unsigned int data_rate_code = 0x0008;
  if(data_rate == 64)  {data_rate_code = 0x0007; ROS_INFO("[x_imu_node] 64 Hz selected");}
  if(data_rate == 128) {                         ROS_INFO("[x_imu_node] 128 Hz selected");}
  if(data_rate == 256) {data_rate_code = 0x0009; ROS_INFO("[x_imu_node] 256 Hz selected");}
  if(data_rate == 512) {data_rate_code = 0x000A; ROS_INFO("[x_imu_node] 512 Hz selected");} 

  ximu.set_register(ximu.REGISTER_ADDRESS_InertialAndMagneticDataRate, data_rate_code);
  ximu.set_register(ximu.REGISTER_ADDRESS_QuaternionDataRate, data_rate_code);

  unsigned int freq;
  // get inertia data output rate:
  if (!ximu.get_register(XIMU::REGISTER_ADDRESS_QuaternionDataRate, &freq)){
    ROS_FATAL("[x_imu_node] ERROR: cant fetch quaternion rate");
    exit(-1);
  }

  unsigned int alg_mode;
  // get AlgorithmMode:
  if (!ximu.get_register(XIMU::REGISTER_ADDRESS_AlgorithmMode, &alg_mode)){
    ROS_FATAL("[x_imu_node] ERROR: cant AlgorithmMode");
    exit(-1);
  }

  if(init_ahrs){
    ROS_INFO("[x_imu_node] Initializing AHRS Algolism...");
    if(!ximu.send_command_algorithm_init()){
      ROS_FATAL("[x_imu_node] ERROR: Failed");
      exit(-1);
    }else{
      ROS_INFO("[x_imu_node] Initializing AHRS Algolism...Done");
    }
    ros::Duration(1.5).sleep(); // wait 1.5sec until initialze will be done and become stable
  }

  if(clear_tare){
    ROS_INFO("[x_imu_node] Clearing default pose...");
    if(!ximu.send_command_algorithm_clear_tare()){
      ROS_FATAL("[x_imu_node] ERROR: Failed");
      exit(-1);
    }else{
      ROS_INFO("[x_imu_node] Clearing default pose...Done");
    }
  }


  unsigned int tare_qx, tare_qy, tare_qz, tare_qw;
  // get AlgorithmMode:
  if (!ximu.get_register(XIMU::REGISTER_ADDRESS_AlgorithmTareQuat0, &tare_qw)){
    ROS_FATAL("[x_imu_node] ERROR: cant Tare num");
    exit(-1);
  }
  if (!ximu.get_register(XIMU::REGISTER_ADDRESS_AlgorithmTareQuat1, &tare_qx)){
    ROS_FATAL("[x_imu_node] ERROR: cant Tare num");
    exit(-1);
  }
  if (!ximu.get_register(XIMU::REGISTER_ADDRESS_AlgorithmTareQuat2, &tare_qy)){
    ROS_FATAL("[x_imu_node] ERROR: cant Tare num");
    exit(-1);
  }
  if (!ximu.get_register(XIMU::REGISTER_ADDRESS_AlgorithmTareQuat3, &tare_qz)){
    ROS_FATAL("[x_imu_node] ERROR: cant Tare num");
    exit(-1);
  }

  
  //ximu.send_command_algorithm_tare();
  //ROS_INFO("[x_imu_node] Initiaize Default Pose...");

  // Qvals of qurtanion is 15
  ROS_INFO("[x_imu_node] Start with \n\t\t\t\t\t\t Port=%s, DeviceID=0x%04X\n\t\t\t\t\t\t Frame=%s, Topic=%s @%dHz, yaw_offset=%f(rad, from North), mode=%s\n\t\t\t\t\t\t Tare qaut=(%f, %f, %f, %f)", 
	   port.c_str(), deviceid, frame.c_str(), topic_name.c_str(), DataOutputRates_int[freq], yaw_offset, AlgorithmModes_str[alg_mode].c_str(),
	   FixedToFloat(tare_qx, 15), FixedToFloat(tare_qy, 15), FixedToFloat(tare_qz, 15), FixedToFloat(tare_qw, 15));

  ros::Rate loop_rate(DataOutputRates_int[freq]);
  //ros::Rate loop_rate(10);
  //int seq_num = 0;

  ROS_INFO("[x_imu_node] Start Loop");
  while (ros::ok())
    {
      double euler[3];
      double gyro[3];
      double accl[3];
      double mag[3];

      sensor_msgs::Imu imu_msg;

      //wait for new data                                                                                                                                                                          
      if(ximu.get_euler(euler) & ximu.get_cal(gyro, accl, mag))
	{
	  geometry_msgs::Quaternion quat;
	  quaternionTFToMsg( tf::createQuaternionFromRPY(euler[0]*3.141593/180.0, euler[1]*3.141593/180.0, euler[2]*3.141593/180.0-yaw_offset), quat );
	  // or
	  //Eigen::Vector3d vec_mag = Eigen::Vector3d(mag[0], mag[1], mag[2]);
	  //Eigen::Vector3d vec_nth = Eigen::Vector3d(1, 0, 0); // North direction on map frame
	  //Eigen::Quaterniond quat;
	  //quat.setFromTwoVectors(vec_mag, vec_nth);

	  imu_msg.header.stamp = ros::Time::now();
	  imu_msg.header.frame_id = frame;
	  imu_msg.orientation = quat;
	  //orientation_covariance // TODO
	  imu_msg.angular_velocity.x = gyro[0]*3.14/180.0;
	  imu_msg.angular_velocity.y = gyro[1]*3.14/180.0;
	  imu_msg.angular_velocity.z = gyro[2]*3.14/180.0;
	  //imu_msg.angular_velocity_covariance //TODO
	  imu_msg.linear_acceleration.x = accl[0];
	  imu_msg.linear_acceleration.y = accl[1];
	  imu_msg.linear_acceleration.z = accl[2];
	  //imu_msg.linear_acceleration_covariance //TODO

	  if(debug_msg) ROS_INFO("Euler = [%f, %f, %f](deg)", euler[0], euler[1], euler[2]);

	  imu_pub.publish(imu_msg);
	}

      ros::spinOnce();

      loop_rate.sleep();
    }


  return 0;
}

